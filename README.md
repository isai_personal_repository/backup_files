# README #

Backup files (Python Script)

### What is this repository for? ###

* This Script copy large a mount of files from remote host to a local one as a backup.

### Setup ###

* All required things to configure and run this script without issues:
* 1.- Python >== 3.6
* 2.- Pipfile: sudo apt-get install python-pip
* 3.- Pipenv for virtual environments: pip3 install pipenv
* Its required setup the custom output log path in line #19 (args).
* 4.- .env File. This file needs to be created at top level root folder project.
  * HOST=x.x.x.x
  * USERNAME=usr
  * PASSWORD=pwd
  * FROM_PUT_PATH=/path/to/get/remote/files
  * TO_PUT_PATH=/local/path/to/store/copied/files
* 5.- Grant permissions to main script file (backup_files.py): sudo chmod 755 filename.py.
  