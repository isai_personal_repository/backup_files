#!/usr/bin/python3
import os, sys, logging, logging.config
from ftplib import FTP

from decouple import config


ROOT_DIR = os.path.join(os.path.dirname(os.path.abspath(__file__)))


def download_files(sftp, file_list, local_backup_folder):
	try:
		backup_path = os.path.expanduser('~')
		logging.info('Current BACKCUP directory: ', backup_path)
		for f in file_list:
			logging.info('Downloading file: {}'.format(f))
			
			# Only for folder inside root project.
			# downloaded = os.path.join(f'{ROOT_DIR}{local_backup_folder}', f) 
			downloaded = os.path.join(f'{backup_path}{local_backup_folder}', f)
			
			new_local_file = open(downloaded, 'wb')
			sftp.retrbinary('RETR %s' % f, new_local_file.write)
		logging.info('All files downloaded successfully!')
	except Exception as e:
		logging.error("Error trying to download files: {}".format(e))

def get_filelist(sftp, **kwargs):
	
	sftp.retrlines('LIST')
	sftp.cwd(kwargs['remote_filefolder'])
	sftp.retrlines('LIST')
	
	# Get current file dir list to be processed:
	filelist = [f for f in sftp.nlst()] # Better for some servers: mlsd()
	download_files(sftp, filelist, config('LOCAL_BACKUP_FOLDER'))

	logging.info('Closing connection . . .')
	sftp.quit()
	logging.info('Connection already closed.')
	logging.info('Process finished.\n')

def connection(**kwargs):
	logging.config.fileConfig(f'{ROOT_DIR}/logconfig.ini')
	try:
		logging.info('Connecting to current Repository for extraction . . .')
		sftp = FTP(host=kwargs['host'], timeout=300)
		sftp.set_debuglevel(2)
		sftp.login(user=kwargs['username'], passwd=kwargs['pwd'])
		# sftp.set_pasv(False) # False as base config.
		logging.info('Successfully connected.')
		return sftp

	except ConnectionError as e:
		logging.error('Error trying to connect to remote host: {}'.format(e))
	except ValueError as e:
		logging.error(e)


if __name__ == "__main__":
	"""Script used to backup files to another inside a 
		remote server."""

	sftp = connection(
		host=config('HOST'),
		username=config('USERNAME'),
		pwd=config('PASSWORD')
	)
	get_filelist(
		sftp,
		remote_filefolder=config('REMOTE_FILEFOLDER')
	)