from ftplib import FTP

import logging

from process.validation import extract_file

def pool(filter_str, current_path):
    try:
        logging.info('Connecting to current Repository for extraction . . .')
        sftp = FTP(host='10.0.0.4', timeout=300)
        sftp.set_debuglevel(2)
        sftp.login(user='pi', passwd='mario bross 2')
        sftp.set_pasv(False)
        logging.info('Successfully connected.')

        sftp.cwd('cuentas_por_cobrar')
        file_list = []
        sftp.retrlines('LIST *' + filter_str + '*', file_list.append)
        extract_file(file_list, sftp, current_path)
        print('Closing connection . . . \n')
        sftp.close()
    except Exception as i:
        print('Error trying to connect to the repository: ', i)
        logging.info('Error trying to connect to the repository: ', i)
