FROM python:3.9.10-alpine3.15

# Labels for the project:
LABEL maintainer="backup_files"

WORKDIR /

#Env variables for python config:
ENV PYTHONDONTWRITTEBYTECODE 1
ENV PYTHONUNBUFFERED 1

RUN apk update\
	&& apk add gcc python3-dev musl-dev 


RUN python -m pip install --upgrade pip
COPY . .
RUN python -m pip install -r requirements.txt


# Commands to run the script inside the container:
CMD ["python", "./backup_skyledger.py"]